<footer>
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-md-offset-3">
                    <div class="footer_content">
                        <div class="footer_image">
                            <img src="<?php the_field('website_logo', 'option'); ?>" alt="">
                        </div>
                        <div class="footer_details">
                            <?php if ( get_field('footer_details', 'option') ) : ?>
                            <?php echo get_field('footer_details', 'option'); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright text-center">
        <?php if ( get_field('copyright_text', 'option') ) : ?>
        <p>
            <?php echo get_field('copyright_text', 'option'); ?>
        </p>
        <?php endif; ?>

    </div>
</footer>
<!-- footer area end -->


<?php wp_footer(); ?>
</body>

</html>