<?php 
function blog_sidebar(){
    
    register_sidebar( array(
        'name' => __( 'Blog Sidebar', 'theme-slug' ),
        'id' => 'sidebar-blog',
        'description' => __( 'Blog wedgets show here', 'theme-slug' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget'  => '</div>',
	'before_title'  => '<h2 class="widgettitle">',
	'after_title'   => '</h2>',
    ) );
}
add_action('widgets_init','blog_sidebar');



class social_widget extends WP_Widget{
    public function __construct(){
        parent::__construct('social_widget','Social Wedget',array(
            'description' => 'This is social wedget'
        ));
        
    }
    public function widget($args, $instance){
        $title = $instance['title'];
        $facebook = $instance['facebook'];
        $twitter = $instance['twitter'];
        $google = $instance['google'];
        $linkedin = $instance['linkedin'];
        $pinterest = $instance['pinterest'];
        echo $args['before_widget'].$args['before_title'].$title.$args['after_title'].$args[after_widget]; 
        echo $args['before_widget'].'<ul class="social_link">
        <li><a href="'.$facebook.'" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
        <li><a href="'.$twitter.'" target="_blank"><i class="fa fa-twitter"></i></a></li>
        <li><a href="'.$google.'" target="_blank"><i class="fa fa-google-plus"></i></a></li>
        <li><a href="'.$linkedin.'" target="_blank"><i class="fa fa-linkedin"></i></a></li>
        <li><a href="'.$pinterest.'" target="_blank"><i class="fa fa-pinterest"></i></a></li>
    </ul>'.$args[after_widget];   
    }
    
    
    
    public function form($instance){ 
        $title = $instance['title'];
        $facebook = $instance['facebook'];
        $twitter = $instance['twitter'];
        $google = $instance['google'];
        $linkedin = $instance['linkedin'];
        $pinterest = $instance['pinterest'];
?>
<p>
    <label for="<?php echo $this->get_field_id('title');?>">Title:</label>
</p>
<p>
    <input type="text" class="widefat" id="<?php echo $this->get_field_id('title');?>" value="<?php echo $title?>" name="<?php echo $this->get_field_name('title');?>">
</p>
<p><label for="<?php echo $this->get_field_id('facebook');?>">Facebook Link:</label></p>
<p><input type="url" class="widefat" name="<?php echo $this->get_field_name('facebook');?>" value="<?php echo $facebook?>" id="<?php echo $this->get_field_id('facebook');?>"></p>

<p><label for="<?php echo $this->get_field_id('twitter');?>">Twitter Link:</label></p>
<p><input type="url" class="widefat" name="<?php echo $this->get_field_name('twitter');?>" value="<?php echo $twitter?>" id="<?php echo $this->get_field_id('twitter');?>"></p>

<p><label for="<?php echo $this->get_field_id('google');?>">Google+ Link:</label></p>
<p><input type="url" class="widefat" name="<?php echo $this->get_field_name('google');?>" value="<?php echo $google?>" id="<?php echo $this->get_field_id('google');?>"></p>

<p><label for="<?php echo $this->get_field_id('linkedin');?>">Linkedin Link:</label></p>
<p><input type="url" class="widefat" name="<?php echo $this->get_field_name('linkedin');?>" value="<?php echo $linkedin?>" id="<?php echo $this->get_field_id('linkedin');?>"></p>

<p><label for="<?php echo $this->get_field_id('pinterest');?>">Pinterest Link:</label></p>
<p><input type="url" class="widefat" name="<?php echo $this->get_field_name('pinterest');?>" value="<?php echo $pinterest?>" id="<?php echo $this->get_field_id('pinterest');?>"></p>


<?php }
}


function social_widget_dev(){
    register_widget('social_widget'); 
}
add_action('widgets_init','social_widget_dev');

?>
