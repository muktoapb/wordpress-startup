<?php 

/*
 ==================
 Theme support
======================	 */


function mukto_bootstraping(){
    load_theme_textdomain('mukto');
    add_theme_support('post-thumbnails');
    add_theme_support( 'title-tag' );
    register_nav_menu('header_menu',__('Header Menu','mukto'));
    register_nav_menu('footer_menu',__('Footer Menu','mukto'));
}
add_action('after_setup_theme','mukto_bootstraping');




/* Adding a language class to the body to apply styles individually per language */

// function mukto_language_class($classes){
//     $classes[] = 'language-'.ICL_LANGUAGE_CODE; 
//     return $classes;
//     }
// add_filter('body_class', 'mukto_language_class');


// Social Share button
function mukto_social_sharing_buttons($content) {
	global $post;
	if(is_singular() || is_home()){
	
		// Get current page URL 
		$muktoURL = urlencode(get_permalink());
 
		// Get current page title
		$muktoTitle = htmlspecialchars(urlencode(html_entity_decode(get_the_title(), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');
		// $muktoTitle = str_replace( ' ', '%20', get_the_title());
		
		// Get Post Thumbnail for pinterest
		$muktoThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
 
		// Construct sharing URL without using any script
		$twitterURL = 'https://twitter.com/intent/tweet?text='.$muktoTitle.'&amp;url='.$muktoURL.'&amp;via=muktoapb';
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$muktoURL;
		$googleURL = 'https://plus.google.com/share?url='.$muktoURL;
		$whatsappURL = 'whatsapp://send?text='.$muktoTitle . ' ' . $muktoURL;
		$linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$muktoURL.'&amp;title='.$muktoTitle;
 
		// Based on popular demand added Pinterest too
		$pinterestURL = 'https://pinterest.com/pin/create/button/?url='.$muktoURL.'&amp;media='.$muktoThumbnail[0].'&amp;description='.$muktoTitle;
 
		// Add sharing button at the end of page/page content
		$content .= '<div class="mukto-social">';
		$content .= '<h5>SHARE ON</h5> <a class="mukto-link mukto-twitter" href="'. $twitterURL .'" target="_blank"><i class="fa-twitter fa"></i></a>';
		$content .= '<a class="mukto-link mukto-facebook" href="'.$facebookURL.'" target="_blank"><i class="fa-facebook fa"></i></a>';
		$content .= '<a class="mukto-link mukto-whatsapp" href="'.$whatsappURL.'" target="_blank"><i class="fa-whatsapp fa"></i></a>';
		$content .= '<a class="mukto-link mukto-googleplus" href="'.$googleURL.'" target="_blank"><i class="fa-google-plus fa"></i></a>';
		$content .= '<a class="mukto-link mukto-linkedin" href="'.$linkedInURL.'" target="_blank"><i class="fa-linkedin fa"></i></a>';
		$content .= '<a class="mukto-link mukto-pinterest" href="'.$pinterestURL.'" data-pin-custom="true" target="_blank"><i class="fa-pinterest fa"></i></a>';
		$content .= '</div>';
		
		return $content;
	}else{
		// if not a post/page then don't include sharing button
		return $content;
	}
};
add_filter( 'the_content', 'mukto_social_sharing_buttons');