<?php

function mukto_css_js(){

	//link css
	//wp_enqueue_style( 'gfont', 'https://fonts.googleapis.com/css?family=Lato:400,700', array(), '1.0', 'all' );
	wp_enqueue_style( 'bootstrap', get_template_directory_uri().'/assets/css/bootstrap.min.css', array(), '1.0', 'all' );
	wp_enqueue_style( 'font-awesome', get_template_directory_uri().'/assets/css/font-awesome.min.css', array(), '1.0', 'all' );
	wp_enqueue_style( 'animate', get_template_directory_uri().'/assets/css/animate.min.3.5.1.css', array(), '1.0', 'all' );
	wp_enqueue_style( 'owl-css', get_template_directory_uri().'/assets/css/owl.css', array(), '1.0', 'all' );
	wp_enqueue_style( 'lightbox-css', get_template_directory_uri().'/assets/css/colorbox.css', array(), '2.0', 'all' );
	wp_enqueue_style( 'main-css', get_template_directory_uri().'/assets/css/style.css', array(), time(), 'all' );
	wp_enqueue_style( 'responsive', get_template_directory_uri().'/assets/css/responsive.css', array(), time(), 'all' );
	
    //	Link Jquery
    wp_enqueue_script( 'jquery_min', get_template_directory_uri().'/assets/js/jquery.min.js', array(), '1.0', true );
    wp_enqueue_script( 'bootstrap', get_template_directory_uri().'/assets/js/bootstrap.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'wow-js', get_template_directory_uri().'/assets/js/wow.min.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'owl', get_template_directory_uri().'/assets/js/owl.js', array('jquery'), '1.0', true );
    wp_enqueue_script( 'light_box', get_template_directory_uri().'/assets/js/colorbox.js', array('jquery'), '2.0', true );
    wp_enqueue_script( 'custom', get_template_directory_uri().'/assets/js/custom.js', array('jquery'), time(), true );
}
add_action('wp_enqueue_scripts','mukto_css_js');