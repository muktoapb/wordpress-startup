<?php
 /*
    Template Name: Contact
    */
 ?>
<?php get_header(); ?>

<div class="contact_content">
    <div class="container">
        <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/page/content', 'page' );

                endwhile; // End of the loop.
            ?>
    </div>
</div>

<?php get_footer();?>