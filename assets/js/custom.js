(function ($) {
    "use strict";

    jQuery(document).ready(function ($) {

        
//            
           $(".menu_btn").click(function () {
            $(".main_nav_area").toggle(300);
        });
        jQuery(window).resize(function(){
        var screenwidth=jQuery(window).width();
        if(screenwidth>767){
            jQuery(".main_nav_area").removeAttr("style");
        }
    });
        
        
        
        // slide big
        $(".slider_wrapper").owlCarousel({
            items: 1,
            nav: true,
            navText: ["<i class='fa fa-long-arrow-left'></i>", "<i class='fa fa-long-arrow-right'></i>"],
            dots: false,
            autoplay: true,
            loop: true,
            mouseDrag: true,
            touchDrag: true,
            animateIn: 'flipInX',
            animateOut: 'fadeOut',
        });  
        // product slider
        $(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390});


        
      
    });

}(jQuery));
