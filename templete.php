<?php
    /*
    Template Name: Home
    */
?>
<?php get_header(); ?>
<div class="hero_area" style="background:url(<?php echo get_field('hero_area_background_image'); ?>);background-position: center;background-size: cover;">
    <div class="container">
        <div class="hero_text">
            <?php if ( get_field('hero_text') ) : ?>
            <?php echo get_field('hero_text'); ?>
            <?php endif; ?>
        </div>
        <?php if ( get_field('button_text') ) : ?>
        <div class="hero_btn">
            <a href="<?php echo get_field('button_link'); ?>" class="site_btn">
                <?php echo get_field('button_text'); ?>
            </a>
        </div>
        <?php endif; ?>
    </div>
</div>
<!--===========\
  Slider Area
   ===================-->
<!-- <section class="product">
    <div class="container">
        <div class="row">
            <div class="section_title">
                <h2 class="text-center">
                    <?php the_field('section_title')?>
                </h2>
            </div>
        </div>
        <div class="product_slider">
            <?php
                $args = array(
                'post_type' => 'product',
                'stock' => 1,
                'posts_per_page' => 9,
                'orderby' =>'date',
                'order' => 'DESC' );
                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
            <div class="product_item">
                <div class="woocommerce product_item_wooc">
                    <div class="product_img">
                        <div class="sels">
                            <?php if(get_field('sale_offer')): ?>
                            <div class="tag">
                                <div class="offer_tag">
                                    <?php the_field('sale_offer')?>
                                </div>
                            </div>
                            <?php endif;?>
                            <?php if(get_field('product_type')): ?>
                            <div class="tag">
                                <div class="product_type">
                                    <?php the_field('product_type')?>
                                </div>
                            </div>
                            <?php endif;?>
                        </div>
                        <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail($loop->post->ID, 'shop_catalog'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="My Image Placeholder" />'; ?>
                        <div class="product_btn">
                            <a href="<?php the_post_thumbnail_url(); ?>" data-lightbox="m_product" data-title="<?php the_title(); ?>"><i
                                    class="fa fa-search-plus"></i></a>
                            <a href="<?php the_permalink(); ?>"><i class="fa fa-shopping-basket"></i></a>
                        </div>
                    </div>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <h3>
                            <?php the_title(); ?>
                        </h3>
                        <p class="price">
                            <?php echo $product->get_price_html(); ?>
                        </p>
                        <span class="woocommerce-product-rating">
                            <?php echo wc_get_rating_html( $product->get_average_rating() ); ?></span>
                    </a>
                </div>
            </div>

            
<?php endwhile; ?>
<?php wp_reset_query(); ?>

</div>
</div>

</section> -->



<!--  ===========\
    Product End
   ===============-->
<div class="service_area">
    <div class="container">
        <div class="title">
            <?php if ( get_field('service_title') ) : ?>
            <h3>
                <?php echo get_field('service_title'); ?>
            </h3>
            <?php endif; ?>
        </div>
        <div class="section_details">
            <?php if ( get_field('service_details') ) : ?>
            <?php echo get_field('service_details'); ?>
            <?php endif; ?>

            <?php if ( get_field('service_button_text') ) : ?>
            <a href="<?php echo get_field('service_button_link'); ?>" class="site_btn">
                <?php echo get_field('service_button_text'); ?>
            </a>
            <?php endif; ?>


        </div>
    </div>
</div>
<div class="product_area">
    <?php if ( have_rows('product_box') ) : ?>

    <?php while( have_rows('product_box') ) : the_row(); ?>
    <div class="product_box_side <?php echo get_sub_field('box_size'); ?>" style="background:<?php echo get_sub_field('box_background_color'); ?>">
        <div class="product_img">
            <?php if ( get_sub_field('product_image') ) : $image = get_sub_field('product_image'); ?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            <?php endif; ?>
        </div>
        <?php if( get_sub_field('product_title') ) : ?>
        <h3>
            <?php echo get_sub_field('product_title'); ?>
        </h3>
        <?php endif; ?>

        <div class="hover_product">
            <div class="hover_product_wrapper">
                <?php if( get_sub_field('product_hover_text') ) : ?>
                <p>
                    <?php echo get_sub_field('product_hover_text'); ?>
                </p>
                <?php endif; ?>

                <?php if( get_sub_field('product_button_text') ) : ?>
                <a href=" <?php echo get_sub_field('product_button_link'); ?>" class="border_btn">
                    <?php echo get_sub_field('product_button_text'); ?></a>
                <?php endif; ?>
            </div>

        </div>
    </div>
    <?php endwhile; ?>

    <?php endif; ?>

</div>
<div class="diveo_area">
    <div class="container">
        <div class="title text-center">
            <?php if ( get_field('video_section_title') ) : ?>
            <h3>
                <?php echo get_field('video_section_title'); ?>
            </h3>
            <?php endif; ?>
        </div>
    </div>
    <?php if ( get_field('video_link') ) : ?>
    <div class="vodeo_wrapper" style="background:url( <?php echo get_field('video_background'); ?>);background-size: cover;background-position: center;">
        <div class="playbtn">
            <a class="youtube cboxElement" href=" <?php echo get_field('video_link'); ?>">
                <?php echo get_field('video_play_button_text'); ?></a>
        </div>
    </div>
    <?php endif; ?>

</div>
<div class="logo_area">
    <div class="container">
        <div class="title text-center">
            <?php if ( get_field('logo_section_title') ) : ?>
            <p>
                <?php echo get_field('logo_section_title'); ?>
            </p>
            <?php endif; ?>

        </div>
        <div class="row">
            <?php if ( have_rows('logos') ) : ?>

            <?php while( have_rows('logos') ) : the_row(); ?>


            <?php if ( get_sub_field('slogo') ) : ?>
            <div class="col-sm-4">
                <div class="s_logo">
                    <img src="<?php the_sub_field('slogo'); ?>" alt="<?php the_sub_field(''); ?>">
                </div>

            </div>
            <?php endif; ?>


            <?php endwhile; ?>

            <?php endif; ?>

        </div>
    </div>
</div>
<div class="help_area">
    <div class="container">
        <div class="question">
            <?php if ( get_field('qsection_text') ) : ?>
            <?php echo get_field('qsection_text'); ?>
            <?php endif; ?>

            <?php if ( get_field('qbutton_text') ) : ?>
            <a href="<?php echo get_field('qbutton_link'); ?>" class="site_btn">
                <?php echo get_field('qbutton_text'); ?>
            </a>
            <?php endif; ?>
        </div>
    </div>
</div>



<!-- <section class="content_area">
    <div class="container">
        <div class="home_content">
            <?php
			while ( have_posts() ) : the_post();
				//get_template_part( 'template-parts/page/content', 'page' );
			endwhile; 
			?>
        </div>
    </div>
</section> -->
<!--  ===========\
    Content End
   ===============-->
<?php get_footer()?>