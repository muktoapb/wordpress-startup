<?php get_header();?>
<div class="main_content_blog">
    <div class="container">
        <div class="row">

            <?php 
                                if(have_posts()):
                                while(have_posts()):the_post();
                            ?>
            <div class="col-sm-6">
                <div class="blog_left_single_item">
                    <div class="blog_pic image_fulwidth">
                        <div class="post_image">
                            <a href="<?php the_permalink();?>">
                                <?php the_post_thumbnail();?></a>
                        </div>

                    </div>
                    <div class="blog_left_single_content para_default">
                        <h3><a href="<?php the_permalink();?>">
                                <?php echo get_the_title(); ?></a></h3>
                        <p class="small">
                            <?php echo get_the_date(); ?>
                        </p>
                        <p>
                            <?php echo wp_trim_words(get_the_content(),35); ?>
                        </p>
                        <div class="btn_explore"><a href="<?php the_permalink();?>">Read More</a></div>
                    </div>
                </div><!-- blog_left_single_item -->
            </div>
            <?php endwhile;?>


            <div class="col-xs-12">
                <div class="blog_pagination">
                    <nav>
                        <ul class="pagination pagination-lg">
                            <?php the_posts_pagination( array(
                                        'mid_size' => 2,
                                        'prev_text' => __( '<i class="flaticon-left-arrow"></i>', 'textdomain' ),
                                        'next_text' => __( '<i class="flaticon-right-arrow"></i>', 'textdomain' ),
                                    ) ); ?>
                        </ul>
                    </nav>
                </div>
            </div>

            <?php //if no post
                            else: 
                                get_template_part('template-parts/page/content','none');
                            endif;
                            wp_reset_postdata();
                        ?>

        </div><!-- row -->
    </div><!-- container -->
</div><!-- blog_page_area -->

<?php get_footer();?>