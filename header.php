<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> data-spy="scroll" data-target=".navigation" data-offset="50">
    <div class="header_area">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="logo">
                        <a href="<?php bloginfo( 'url' )?>">
                            <img src="<?php the_field('website_logo', 'option'); ?>" alt="">
                        </a>
                    </div>
                    <div class="menu_btn">
                        <i class="fa fa-bars"></i>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="main_nav_area">
                        <div class="navigation">
                            <?php wp_nav_menu(array(
                                    'theme_location' => 'header_menu',
                                    'container' => 'nav',
                                    'container_class' => 'nav-collapse',
                                    'menu_class' => 'main-menu',
                                    'menu_id' => 'menu',
                                    'depth' => '3',
                                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                                    'walker' => new WP_Bootstrap_Navwalker()
                                )); 
                                ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header area End
=========================== -->