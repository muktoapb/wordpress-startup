<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>

    <div class="container">
        <div class="product_image">
            <?php
		/**
		 * Hook: woocommerce_before_single_product_summary.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>
        </div>

        <div class="summary entry-summary">
            <?php echo woocommerce_template_single_title() ?>
            <?php if ( get_field('reference') ) : ?>
            <p> <b>Reference :
                    <?php echo get_field('reference'); ?></b>
            </p>
            <?php endif; ?>

            <?php echo woocommerce_template_single_price() ?>
            <?php echo woocommerce_template_single_excerpt() ?>
            <?php echo woocommerce_template_single_add_to_cart() ?>
            <?php echo woocommerce_template_single_sharing() ?>
            <?php
			/**
			 * Hook: woocommerce_single_product_summary.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * 
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */
			//do_action( 'woocommerce_single_product_summary' );
		?>
        </div>
    </div>

    <?php
		/**
		 * Hook: woocommerce_after_single_product_summary.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		//do_action( 'woocommerce_after_single_product_summary' );
	?>



    <?php if ( have_rows('bottom_slider', 'option') ) : ?>

    <div class="testimonial">
        <div class="container">
            <div class="slider_wrapper owl-carousel">
                <?php while( have_rows('bottom_slider', 'option') ) : the_row(); ?>
                <div class="testionial_Slider">
                    <?php the_sub_field('slider_text', 'option'); ?>
                </div>
                <?php endwhile; ?>
            </div>

        </div>
    </div>
    <?php endif; ?>

    <div class="product_logo_area">
        <div class="container">
            <div class="row">
                <?php if ( have_rows('sponsor_logos', 'option') ) : ?>

                <?php while( have_rows('sponsor_logos', 'option') ) : the_row(); ?>


                <?php if ( get_sub_field('logo', 'option') ) : ?>
                <div class="col-sm-4">
                    <div class="s_logo">
                        <img src="<?php the_sub_field('logo', 'option'); ?>" alt="<?php the_sub_field(''); ?>">
                    </div>

                </div>
                <?php endif; ?>


                <?php endwhile; ?>

                <?php endif; ?>

            </div>
        </div>
    </div>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>