<?php get_header(); ?>
<div class="entry-header">
    <div class="container">
        <h1 class="woocommerce-products-header__title page-title">
            <?php the_title(); ?>
        </h1>
        <div class="post_thumnnail">
            <img src="<?php the_post_thumbnail_url();?>" alt="">
        </div>
    </div>
</div>

<div class="main_content_blog">
    <div class="container">
        <?php 
                                if(have_posts()):
                                while(have_posts()):the_post();
                            ?>
        <div class="blog_left_side_area">


            <div class="blog_left_single_content blog_single_content para_default">
                <h3>
                    <?php echo get_the_title(); ?>
                </h3>
                <p>
                    <?php echo the_content(); ?>
                </p>
            </div>

            <div class="blog_tag">
                <?php the_tags( '','','');?>
            </div>
            <div class="content_blog_a fix">
                <div class="e_blog_A">
                    <?php echo get_avatar(get_the_author_meta('ID'),100);?>
                </div>
                <div class="blog_a_text">
                    <span>Author : </span><a href="<?php get_author_posts_url(get_the_author_meta('ID'),get_the_author_meta('user_nicename'));?>">
                        <?php the_author()?>
                    </a>
                    <p>
                        <?php the_author_meta('description');?>
                    </p>
                </div>
            </div>



            <?php
		                  	if ( comments_open() || get_comments_number() ) :
					comments_template();
                        endif;
                    ?>

        </div>
        <!-- blog_left_side_area -->
        <?php 
                            endwhile;
                             endif;
                            wp_reset_postdata();
                        ?>






    </div>
    <!-- container -->
</div>
<!-- blog_page_area -->

<?php get_footer();?>